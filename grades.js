javascript: (function () {
  const elements = [
    ...document.querySelector(".list").children[0].children,
  ].filter((element) =>
    ["listrowodd", "listroweven"].includes(element.className)
  );

  let points = 0;
  let possiblePoints = 0;
  for (const element of elements) {
    const nums = element.cells[5].innerText.split("\n")[0].split(" / ");

    const if_Not_NaN = function (num) {
      const number = parseFloat(num);
      if (isNaN(number)) {
        return 0;
      }
      return number;
    };

    points += if_Not_NaN(nums[0]);
    possiblePoints += if_Not_NaN(nums[1]);
  }
  alert(`${points} / ${possiblePoints}`);
})();
