# Grade Total

This projects totals your points for your grade on the `Genesis Parent Portal` website.

## How to Implement

Add a new bookmark with this text as the URL:

```Javascript
javascript:!function(){let e=[...document.querySelector(".list").children[0].children,].filter(e=>["listrowodd","listroweven"].includes(e.className)),l=0,t=0;for(let i of e){let n=i.cells[5].innerText.split("\n")[0].split(" / "),r=function(e){let l=parseFloat(e);return isNaN(l)?0:l};l+=r(n[0]),t+=r(n[1])}alert(`${l} / ${t}`)}();
```

## How to Use

Click on the bookmark while you are looking at your grade details for a specific class.
